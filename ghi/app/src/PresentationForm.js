import React, {useState, useEffect} from 'react';



function PresentationForm(){

  const[conferences, setConferences] = useState([])
  const [formData, setFormData] = useState({
    presenter_name: "",
    presenter_email: "",
    company_name: "",
    title: "",
    synopsis: "",
    conference: "",
  })

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();


    const url = `http://localhost:8000/api/conferences/${formData.conference}/presentations/`;

    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: '',
      })
    }
  }



  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }


  return (

    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new presentation</h1>
        <form  onSubmit ={handleSubmit} id="create-presentation-form">
          <div className="form-floating mb-3">
            <input placeholder="Presenter name"
            required type="text"
            value = {formData.presenter_name}
            onChange={handleFormChange}
            name="presenter_name"
            id="presenter_name"
            className="form-control">
            </input>
            <label htmlFor="presenter_name">Presenter name</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Presenter email"
            value = {formData.presenter_email}
            onChange={handleFormChange}
            required type="email"
            name="presenter_email"
            id="presenter_email"
            className="form-control"></input>
            <label htmlFor="presenter_email">Presenter email</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Company name"
            type="text"
            onChange={handleFormChange}
            name="company_name"
            value = {formData.company_name}
            id="company_name"
            className="form-control"></input>
            <label htmlFor="company_name">Company name</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Title"
            required type="text"
            name="title"
            value = {formData.title}
            onChange={handleFormChange}
            id="title"
            className="form-control"></input>
            <label htmlFor="title">Title</label>
          </div>
          <div className="mb-3">
            <label htmlFor="synopsis">Synopsis</label>
            <textarea className="form-control"
            id="synopsis"
            value = {formData.synopsis}
            onChange={handleFormChange}
            rows="3"
            name="synopsis"
            />
          </div>
          <div className="mb-3">
            <select onChange={handleFormChange}
            required name="conference"
            value = {formData.conference}
            className="form-select">
              <option value="">Choose a conference</option>
              {conferences.map(conference => {
                return(
                  <option key={conference.id} value={conference.id}>{conference.name}</option>
                )
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
    // your JSX code here
  );

};
export default PresentationForm;
